from unittest import TestCase

from yandex_music_agent.common.srcset import SrcSet


class SrcSetTester(TestCase):

    def test(self):
        srcset = SrcSet.parse("//avatars.yandex.net/get-music-content/113160/b79f0f95.a.4912261-1/200x200, //avatars.yandex.net/get-music-content/113160/b79f0f95.a.4912261-1/400x400 2x")
        self.assertEqual(2, len(srcset.values))
        self.assertEqual(2, srcset.best.size)
