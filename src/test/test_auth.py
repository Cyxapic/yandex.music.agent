from unittest import TestCase

from yandex_music_agent import auth
from yandex_music_agent.auth import AuthException
from yandex_music_agent.main import Credentials


class AuthTester(TestCase):

    def test(self):
        credentials = Credentials()
        cookie = auth.resolve_cookie(credentials.login, credentials.password)
        self.assertTrue(cookie)
        self.assertIsNotNone(cookie.uid)
        self.assertIsNotNone(cookie.login)

    def test_fail(self):
        with self.assertRaises(AuthException):
            auth.resolve_cookie("test", "test")
